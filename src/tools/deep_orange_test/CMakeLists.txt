# Copyright 2021 The Autoware Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

cmake_minimum_required(VERSION 3.5)

project(deep_orange_test)

# require that dependencies from package.xml be available
find_package(ament_cmake_auto REQUIRED)
ament_auto_find_build_dependencies(REQUIRED
  ${${PROJECT_NAME}_BUILD_DEPENDS}
  ${${PROJECT_NAME}_BUILDTOOL_DEPENDS}
)

set(DEEP_ORANGE_TEST_LIB_SRC
  src/deep_orange_test.cpp
)

set(DEEP_ORANGE_TEST_LIB_HEADERS
  include/deep_orange_test/deep_orange_test.hpp
  include/deep_orange_test/visibility_control.hpp
)

# generate library
ament_auto_add_library(${PROJECT_NAME} SHARED
  ${DEEP_ORANGE_TEST_LIB_SRC}
  ${DEEP_ORANGE_TEST_LIB_HEADERS}
)
autoware_set_compile_options(${PROJECT_NAME})

set(DEEP_ORANGE_TEST_NODE_SRC
  src/deep_orange_test_node.cpp
)

set(DEEP_ORANGE_TEST_NODE_HEADERS
  include/deep_orange_test/deep_orange_test_node.hpp
)

# generate component node library
ament_auto_add_library(${PROJECT_NAME}_node SHARED
  ${DEEP_ORANGE_TEST_NODE_SRC}
  ${DEEP_ORANGE_TEST_NODE_HEADERS}
)
autoware_set_compile_options(${PROJECT_NAME}_node)
rclcpp_components_register_node(${PROJECT_NAME}_node
  PLUGIN "autoware::deep_orange_test::DeepOrangeTestNode"
  EXECUTABLE ${PROJECT_NAME}_node_exe
)

# Testing
if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  ament_lint_auto_find_test_dependencies()

  # Unit tests
  set(TEST_SOURCES test/test_deep_orange_test.cpp)
  set(TEST_DEEP_ORANGE_TEST_EXE test_deep_orange_test)
  ament_add_gtest(${TEST_DEEP_ORANGE_TEST_EXE} ${TEST_SOURCES})
  autoware_set_compile_options(${TEST_DEEP_ORANGE_TEST_EXE})
  target_link_libraries(${TEST_DEEP_ORANGE_TEST_EXE} ${PROJECT_NAME})
endif()

# ament package generation and installing
ament_auto_package(INSTALL_TO_SHARE
  launch
)
