// Copyright 2021 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// \copyright Copyright 2021 The Autoware Foundation
/// \file
/// \brief This file defines the deep_orange_test class.

#ifndef DEEP_ORANGE_TEST__DEEP_ORANGE_TEST_HPP_
#define DEEP_ORANGE_TEST__DEEP_ORANGE_TEST_HPP_

#include <deep_orange_test/visibility_control.hpp>

#include <cstdint>

namespace autoware
{
/// \brief TODO(anshums): Document namespaces!
namespace deep_orange_test
{

/// \brief TODO(anshums): Document your functions
int32_t DEEP_ORANGE_TEST_PUBLIC print_hello();

}  // namespace deep_orange_test
}  // namespace autoware

#endif  // DEEP_ORANGE_TEST__DEEP_ORANGE_TEST_HPP_
