# Copyright 2020-2021 The Autoware Foundation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Testing of purepursuit_controller in LGSVL simulation using recordreplay planner."""

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from ament_index_python import get_package_share_directory
import os


def get_share_file(package_name, file_name):
    return os.path.join(get_package_share_directory(package_name), file_name)


def generate_launch_description():
    """
    Launch nodes with params to test record_replay_planner with pure pursuit in simulation.

    pure_pursuit_controller + LGSVL + recordreplay planner + lidar osbtacle ddetection
    """
    # --------------------------------- Params -------------------------------
    joy_translator_param_file = get_share_file(
        package_name='joystick_vehicle_interface_nodes', file_name='param/logitech_f310.param.yaml')
    ssc_interface_param_file = get_share_file(
        package_name='ssc_interface', file_name='param/defaults.param.yaml')
    # pt_controller_param_file = get_share_file(
    #     package_name='pt_controller', file_name='param/___.param.yaml')
    # gnss_parma_file = get_share_file(
    #     package_name='gnss_package_name', file_name='param/___.param.yaml')
    # ros_can_launch_file = get_share_file(
    #     package_name='gnss_package_name', file_name='param/___.param.yaml')
    recordreplay_param_file = get_share_file(
        package_name='recordreplay_planner_nodes', file_name='param/defaults.param.yaml')
    

    # --------------------------------- Arguments -------------------------------
    joy_translator_param = DeclareLaunchArgument(
        'joy_translator_param_file',
        default_value=joy_translator_param_file,
        description='Path to config file for joystick translator'
    )
    ssc_interface_param = DeclareLaunchArgument(
        'ssc_interface_param_file',
        default_value=ssc_interface_param_file,
        description='Path to config file for ssc_interface'
    )
    # pt_controller_param = DeclareLaunchArgument(
    #     'pt_controller_param_file',
    #     default_value=pt_controller_param_file,
    #     description='Path to config file for joystick translator'
    # )
    # gnss_param = DeclareLaunchArgument(
    #     'gnss_param_file',
    #     default_value=gnss_param_file,
    #     description='Path to config file for joystick translator'
    # )
    # ros_can_param = DeclareLaunchArgument(
    #     'ros_can_launch_file',
    #     default_value=ros_can_launch_file,
    #     description='Path to config file for joystick translator'
    # )
    recordreplay_param = DeclareLaunchArgument(
        'recordreplay_param_file',
        default_value=recordreplay_param_file,
        description='Path to config file for joystick translator'
    )
    
   

    # -------------------------------- Nodes-----------------------------------

    joystick_launch_file_path = get_share_file(
        package_name='joystick_vehicle_interface_nodes',
        file_name='launch/joystick_vehicle_interface_node.launch.py')
    joystick = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(joystick_launch_file_path),
        launch_arguments=[
            ("joy_translator_param", LaunchConfiguration('joy_translator_param_file'))]
    )

    ssc_interface_launch_file_path = get_share_file(
        package_name='ssc_interface',
        file_name='launch/ssc_interface.launch.py')
    ssc_interface = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(ssc_interface_launch_file_path),
        launch_arguments=[
            ("ssc_interface_param", LaunchConfiguration('ssc_interface_param_file'))]
    )

    # pt_controller_launch_file_path = get_share_file(
    #     package_name='pt_controller',
    #     file_name='launch/pt_controller.launch.py')
    # pt_controller = IncludeLaunchDescription(
    #     PythonLaunchDescriptionSource(joystick_launch_file_path),
    #     launch_arguments=[
    #         ("pt_controller_param", LaunchConfiguration('pt_controller_param_file'))]
    # )

    # ros_can_launch_file_path = get_share_file(
    #     package_name='ros_can_package',
    #     file_name='launch/____.launch.py')
    # ros_can = IncludeLaunchDescription(
    #     PythonLaunchDescriptionSource(joystick_launch_file_path),
    #     launch_arguments=[
    #         ("gnss_param", LaunchConfiguration('gnss_parma_file'))]
    # )

    # gnss_launch_file_path = get_share_file(
    #     package_name='gnss_package_name',
    #     file_name='launch/____.launch.py')
    # gnss = IncludeLaunchDescription(
    #     PythonLaunchDescriptionSource(joystick_launch_file_path),
    #     launch_arguments=[
    #         ("gnss_param", LaunchConfiguration('gnss_parma_file'))]
    # )


 
    recordreplay_planner_path = get_share_file(
        package_name='recordreplay_planner_nodes',
        file_name='launch/recordreplay_planner_node.launch.py')
    recordreplay_planner_node = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(recordreplay_planner_path)
    )
 
    return LaunchDescription([
        joy_translator_param,
        joystick,
        ssc_interface_param,
        ssc_interface,
        recordreplay_param,
        recordreplay_planner_node

    ])
