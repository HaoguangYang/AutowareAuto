#include <deep_orange_msgs/msg/misc_report.hpp>
#include <deep_orange_msgs/msg/ct_report.hpp>
#include <thread>
#include <functional>

#include <rclcpp/rclcpp.hpp>

class MonitorNode : public rclcpp::Node
{
public:
  /// ROS 2 parameter constructor
  explicit MonitorNode(const rclcpp::NodeOptions & node_options = rclcpp::NodeOptions())
    : rclcpp::Node {"iac_tools_cpp", node_options}, sleeper(rclcpp::Duration::from_seconds(2.0))
  {
    ct_state_descriptions = {"Uninitialized", "Power On", "Initialized", "Actuator Test", "Crank Ready", "Start Engine now",
                            "Race Ready", "Initialize Driving", "Caution", "Normal Race", "Coordinate Stop", "Control Shut Down",
                            "Emergency Shut Down", "Safe", "Failure"};
    
    sys_state_descriptions = {"Uninitialized", "Power On", "Connection Establish", "Actuator Test", 
                              "Actuator test pass", "Wait to Crank", "Crank Check Passed", "Crank", 
                              "Engine Idle", "Driving", "Engine Shut", "Power Off", "Raptor off", 
                              "Crank Check Initialization", "Actuator Test Fail", "Crank Check Fail",
                              "Engine Shut Down", "Engine Failed to start"};
  }
  virtual ~MonitorNode()
  {
    this->stop();
  }
  void stop()
  {
    running_=false;
    log_thread.join();
  }
  void start()
  {
    ct_subscription_ = this->create_subscription<deep_orange_msgs::msg::CtReport>("/raptor_dbw_interface/ct_report", 1, std::bind(&MonitorNode::ctStateCB_, this, std::placeholders::_1));
    misc_report_subscription_ = this->create_subscription<deep_orange_msgs::msg::MiscReport>("/raptor_dbw_interface/misc_report_do", 1, std::bind(&MonitorNode::miscReportCB_, this, std::placeholders::_1));
    log_thread = std::thread(std::bind(&MonitorNode::workerFunc, this));
  }

private:
  deep_orange_msgs::msg::CtReport current_ct_report_;  
  deep_orange_msgs::msg::MiscReport current_misc_report_;  
  std::thread log_thread;
  bool running_;
  rclcpp::Subscription<deep_orange_msgs::msg::CtReport>::SharedPtr ct_subscription_;
  rclcpp::Subscription<deep_orange_msgs::msg::MiscReport>::SharedPtr misc_report_subscription_;
  rclcpp::Duration sleeper;
  std::vector<std::string> ct_state_descriptions;
  std::vector<std::string> sys_state_descriptions;
  void workerFunc()
  {
    while(running_)
    {
      std::string ct_description;
      if (this->current_ct_report_.ct_state==255)
      {
        ct_description = "Default";
      }
      else if (this->current_ct_report_.ct_state>=0 && this->current_ct_report_.ct_state<ct_state_descriptions.size())
      {
        ct_description = ct_state_descriptions[this->current_ct_report_.ct_state];
      }
      else
      {
        ct_description = "Unknown";
      }

      std::string sys_description;
      if (this->current_misc_report_.sys_state==255)
      {
        sys_description = "Default";
      }
      else if (this->current_misc_report_.sys_state>=0 && this->current_misc_report_.sys_state<sys_state_descriptions.size())
      {
        sys_description = sys_state_descriptions[this->current_misc_report_.sys_state];
      }
      else
      {
        sys_description = "Unknown";
      }
      rclcpp::sleep_for(std::chrono::nanoseconds((int)std::floor(0.5E9)));
      RCLCPP_INFO(get_logger(), "\n Current CT State: %u, %s\n.Current SYS State: %u, %s", this->current_ct_report_.ct_state, ct_description.c_str(), this->current_misc_report_.sys_state, sys_description.c_str());
      
      // sleeper.sleep();
    }

  }

  void ctStateCB_(const deep_orange_msgs::msg::CtReport::SharedPtr msg)
  {
    this->current_ct_report_ = *msg;
  }

  void miscReportCB_(const deep_orange_msgs::msg::MiscReport::SharedPtr msg)
  {
    this->current_misc_report_ = *msg;
  }
};

int main(int argc, char** argv)
{   
    rclcpp::init(argc, argv);

    std::shared_ptr<MonitorNode> node(new MonitorNode);
    rclcpp::ExecutorOptions opshinz;
    rclcpp::executors::MultiThreadedExecutor executor(opshinz, 2);
    executor.add_node(node);
    node->start();
    executor.spin();
    node->stop();
}