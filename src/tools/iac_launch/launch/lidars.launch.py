from ament_index_python import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.conditions import IfCondition
from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration
from launch.substitutions import TextSubstitution

#enable_cameras  = False
#enable_radar    = False

def generate_launch_description():


    fp_lidar_front_arg = DeclareLaunchArgument(
        'fp_lidar_front', default_value=TextSubstitution(text='10020')
    )
    fp_lidar_right_arg = DeclareLaunchArgument(
        'fp_lidar_right', default_value=TextSubstitution(text='10021')
    )
    fp_lidar_left_arg = DeclareLaunchArgument(
        'fp_lidar_left', default_value=TextSubstitution(text='10022')
    )

    pkg_dir = get_package_share_directory('iac_launch')

    front_lidar_launch = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            pkg_dir + '/launch/h3_f.launch.py'
        )
    )

    left_lidar_launch = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            pkg_dir + '/launch/h3_lr.launch.py'
        )
    )

    right_lidar_launch = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            pkg_dir + '/launch/h3_rr.launch.py'
        )
    )

    launch_description = [
        fp_lidar_front_arg,
        fp_lidar_right_arg,
        fp_lidar_left_arg,
        front_lidar_launch,
        left_lidar_launch,
        right_lidar_launch
    ]

    return LaunchDescription(launch_description) #, param_declarations])
