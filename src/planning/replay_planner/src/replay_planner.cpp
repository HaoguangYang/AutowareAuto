// Copyright 2020 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "replay_planner/replay_planner.hpp"

#include <common/types.hpp>
#include <geometry/common_2d.hpp>
#include <geometry_msgs/msg/point32.hpp>
#include <motion_common/motion_common.hpp>
#include <time_utils/time_utils.hpp>

#include <algorithm>
#include <cmath>
#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <vector>

using autoware::common::types::bool8_t;
using autoware::common::types::char8_t;
using autoware::common::types::uchar8_t;
using autoware::common::types::float32_t;
using autoware::common::types::float64_t;
using Csv = std::vector<std::vector<std::string>>;
using Association = std::map<std::string /* label */, int32_t /* row */>;


namespace
{
std::vector<std::string> split(const std::string & input, char delimiter)
{
  std::istringstream stream(input);
  std::string field;
  std::vector<std::string> result;
  while (std::getline(stream, field, delimiter)) {
    result.push_back(field);
  }
  return result;
}

void deleteHeadSpace(std::string & string)
{
  while (string.find_first_of(' ') == 0) {
    string.erase(string.begin());
    if (string.empty()) {
      break;
    }
  }
}

void deleteUnit(std::string & string)
{
  size_t start_pos, end_pos;
  start_pos = string.find_first_of('[');
  end_pos = string.find_last_of(']');
  if (start_pos != std::string::npos && end_pos != std::string::npos && start_pos < end_pos) {
    string.erase(start_pos, (end_pos + 1) - start_pos);
  }
}

bool loadData(
  const std::string & file_name, Association & label_row_association_map,
  Csv & file_data,
  const rclcpp::Logger & logger)
{
  file_data.clear();
  label_row_association_map.clear();
  std::ifstream ifs(file_name);

  // open file
  if (!ifs) {
    RCLCPP_FATAL(
      logger,
      "Could not load %s",
      file_name.c_str());
    return false;
  }

  // create label-row association map
  std::string line;
  if (std::getline(ifs, line)) {
    std::vector<std::string> str_vec = split(line, ',');
    for (size_t i = 0; i < str_vec.size(); ++i) {
      deleteUnit(str_vec.at(i));
      deleteHeadSpace(str_vec.at(i));
      label_row_association_map[str_vec.at(i)] = static_cast<int>(i);
    }
  } else {
    RCLCPP_FATAL(
      logger,
      "cannot create association map.");
    return false;
  }

  // create file data
  while (std::getline(ifs, line)) {
    std::vector<std::string> str_vec = split(line, ',');
    file_data.push_back(str_vec);
  }
  return true;
}
}  // namespace


namespace autoware
{
namespace replay_planner
{

using motion::motion_common::to_angle;

ReplayPlanner::ReplayPlanner()
: m_logger{rclcpp::get_logger("ReplayPlanner")}
{
}

void ReplayPlanner::clear_record() noexcept
{
  m_record_buffer.clear();
}

std::size_t ReplayPlanner::get_record_length() const noexcept
{
  return m_record_buffer.size();
}


void ReplayPlanner::set_heading_weight(float64_t heading_weight)
{
  if (heading_weight < 0.0) {
    throw std::domain_error{"Negative weights do not make sense"};
  }
  m_heading_weight = heading_weight;
}

float64_t ReplayPlanner::get_heading_weight()
{
  return m_heading_weight;
}

void ReplayPlanner::set_min_record_distance(float64_t min_record_distance)
{
  if (min_record_distance < 0.0) {
    throw std::domain_error{"Negative minumum distance do not make sense"};
  }
  m_min_record_distance = min_record_distance;
}

float64_t ReplayPlanner::get_min_record_distance() const
{
  return m_min_record_distance;
}

const Trajectory & ReplayPlanner::plan(const State & current_state)
{
  return from_record(current_state);
}


std::size_t ReplayPlanner::get_closest_state(const State & current_state)
{
  // Find the closest point to the current state in the stored states buffer
  const auto distance_from_current_state =
    [this, &current_state](State & other_state) {
      const auto s1 = current_state.state, s2 = other_state.state;
      return (s1.x - s2.x) * (s1.x - s2.x) + (s1.y - s2.y) * (s1.y - s2.y) +
             static_cast<float32_t>(m_heading_weight) * std::abs(to_angle(s1.heading - s2.heading));
    };
  const auto comparison_function =
    [&distance_from_current_state](State & one, State & two)
    {return distance_from_current_state(one) < distance_from_current_state(two);};


  const auto minimum_index_iterator =
    std::min_element(
    std::begin(m_record_buffer), std::end(m_record_buffer),
    comparison_function);
  auto minimum_idx = std::distance(std::begin(m_record_buffer), minimum_index_iterator);

  return static_cast<std::size_t>(minimum_idx);
}


const Trajectory & ReplayPlanner::from_record(const State & current_state)
{
  const auto record_length = get_record_length();
  // Find out where on the recorded buffer we should start replaying
  std::size_t La = static_cast<std::size_t>(0);
  m_traj_start_idx = (get_closest_state(current_state) + La) % record_length;

  // Determine how long the published trajectory will be
  auto & trajectory = m_trajectory;

  m_traj_end_idx =
    std::min(
    {record_length - m_traj_start_idx, trajectory.points.max_size(),
    }) + m_traj_start_idx;
  // loop path mod start
  if (get_mode() == 2) {
    // only if mode is oval the points need to be fed in circular way
    m_traj_end_idx = trajectory.points.max_size() + m_traj_start_idx;
  }

  m_traj_end_idx = m_traj_end_idx % record_length;
  std::size_t index;  // index to m_record_buffer array

  // Assemble the trajectory as desired
  trajectory.header = current_state.header;

  auto publication_len = (m_traj_end_idx > m_traj_start_idx) ?
    m_traj_end_idx - m_traj_start_idx : record_length - m_traj_start_idx;
  if (get_mode() == 2) {
    // only if mode is race the points need to be fed in circular way
    publication_len = trajectory.points.max_size();
  }


  trajectory.points.resize(publication_len);

  const auto t0 = time_utils::from_message(m_record_buffer[m_traj_start_idx].header.stamp);

  for (std::size_t i = {}; i < publication_len; ++i) {
    // Make the time spacing of the points match the recorded timing
    index = (m_traj_start_idx + i) % record_length;
    trajectory.points[i] = m_record_buffer[index].state;
    trajectory.points[i].longitudinal_velocity_mps = get_speed();  // setting desired speed
    trajectory.points[i].time_from_start = time_utils::to_message(
      time_utils::from_message(m_record_buffer[index].header.stamp) - t0);
  }

  // Adjust time stamp from velocity
  float32_t t = 0.0;
  for (std::size_t i = 1; i < publication_len; ++i) {
    auto & p0 = trajectory.points[i - 1];
    auto & p1 = trajectory.points[i];
    auto dx = p1.x - p0.x;
    auto dy = p1.y - p0.y;
    auto v = 0.5f * (p0.longitudinal_velocity_mps + p1.longitudinal_velocity_mps);
    t += std::sqrt(dx * dx + dy * dy) / std::max(std::fabs(v), 1.0e-5f);
    float32_t t_s = 0;
    float32_t t_ns = std::modf(t, &t_s) * 1.0e9f;
    trajectory.points[i].time_from_start.sec = static_cast<int32_t>(t_s);
    trajectory.points[i].time_from_start.nanosec = static_cast<uint32_t>(t_ns);
  }

  // loop path mod end

  // Mark the last point along the trajectory as "stopping" by setting all rates,
  // accelerations and velocities to zero. TODO(s.me) this is by no means
  // guaranteed to be dynamically feasible. One could implement a proper velocity
  // profile here in the future.
  // uncomment section to undo loop path mod

  if (m_traj_end_idx > m_traj_start_idx && get_mode() == 3) {
    // const auto traj_last_idx = m_traj_end_idx - 1U;
    const auto traj_last_idx = trajectory.points.size() - 1U;
    RCLCPP_INFO(m_logger, "traj last idx %lu", traj_last_idx);
    trajectory.points[traj_last_idx].longitudinal_velocity_mps = 0.0;
    trajectory.points[traj_last_idx].lateral_velocity_mps = 0.0;
    trajectory.points[traj_last_idx].acceleration_mps2 = 0.0;
    trajectory.points[traj_last_idx].heading_rate_rps = 0.0;
  }

  return trajectory;
}


void ReplayPlanner::record_state(const State & state_to_record)
{
  if (m_record_buffer.empty()) {
    m_record_buffer.push_back(state_to_record);
    return;
  }

  auto previous_state = m_record_buffer.back();
  auto distance_sq = (state_to_record.state.x - previous_state.state.x) *
    (state_to_record.state.x - previous_state.state.x) +
    (state_to_record.state.y - previous_state.state.y) *
    (state_to_record.state.y - previous_state.state.y);

  if (static_cast<float64_t>(distance_sq) >= (m_min_record_distance * m_min_record_distance) ) {
    m_record_buffer.push_back(state_to_record);
  }
}

void ReplayPlanner::readTrajectoryBufferFromFile(const std::string & replay_path)
{
  RCLCPP_INFO(m_logger, "path is %s", replay_path.c_str());
  if (replay_path.empty()) {
    throw std::runtime_error("replay_path cannot be empty");
  }

  // Clear current trajectory deque
  clear_record();
  Csv file_data;
  Association map;  // row labeled Association map
  if (!loadData(replay_path, map, file_data, m_logger)) {
    RCLCPP_FATAL(
      m_logger,
      "failed to open file : %s",
      replay_path.c_str());
    return;
  }
  for (size_t i = 0; i < file_data.size(); ++i) {
    State s;
    for (size_t j = 0; j < file_data.at(i).size(); ++j) {
      const int _j = static_cast<int>(j);
      if (map.at("t_sec") == _j) {
        s.state.time_from_start.sec = std::stoi(file_data.at(i).at(j));
      } else if (map.at("t_nanosec") == _j) {
        s.state.time_from_start.nanosec = static_cast<uint32_t>(std::stoi(file_data.at(i).at(j)));
      } else if (map.at("x") == _j) {
        s.state.x = std::stof(file_data.at(i).at(j));
      } else if (map.at("y") == _j) {
        s.state.y = std::stof(file_data.at(i).at(j));
      } else if (map.at("heading_real") == _j) {
        s.state.heading.real = std::stof(file_data.at(i).at(j));
      } else if (map.at("heading_imag") == _j) {
        s.state.heading.imag = std::stof(file_data.at(i).at(j));
      } else if (map.at("longitudinal_velocity_mps") == _j) {
        s.state.longitudinal_velocity_mps = std::stof(file_data.at(i).at(j));
      } else if (map.at("lateral_velocity_mps") == _j) {
        s.state.lateral_velocity_mps = std::stof(file_data.at(i).at(j));
      } else if (map.at("acceleration_mps2") == _j) {
        s.state.acceleration_mps2 = std::stof(file_data.at(i).at(j));
      } else if (map.at("heading_rate_rps") == _j) {
        s.state.heading_rate_rps = std::stof(file_data.at(i).at(j));
      } else if (map.at("front_wheel_angle_rad") == _j) {
        s.state.front_wheel_angle_rad = std::stof(file_data.at(i).at(j));
      } else if (map.at("rear_wheel_angle_rad") == _j) {
        s.state.rear_wheel_angle_rad = std::stof(file_data.at(i).at(j));
      }
    }
    record_state(s);
  }
  RCLCPP_INFO(m_logger, "size of m_record_buffer %lu", m_record_buffer.size());
}

void ReplayPlanner::set_mode(int desired_mode)
{
  mode = desired_mode;
}

int ReplayPlanner::get_mode()
{
  return mode;
}

void ReplayPlanner::set_speed(float desired_speed)
{
  speed = desired_speed;
}

float ReplayPlanner::get_speed()
{
  return speed;
}

void ReplayPlanner::IncrementA()
{
  a++;
}

}  // namespace replay_planner
}  // namespace autoware
