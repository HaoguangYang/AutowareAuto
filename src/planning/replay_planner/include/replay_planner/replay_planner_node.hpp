// Copyright 2020 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// \copyright Copyright 2020 The Autoware Foundation
/// \file
/// \brief This file defines the replay_planner_node class.

#ifndef REPLAY_PLANNER__REPLAY_PLANNER_NODE_HPP_
#define REPLAY_PLANNER__REPLAY_PLANNER_NODE_HPP_

#include <replay_planner/replay_planner.hpp>
#include <autoware_auto_msgs/msg/trajectory.hpp>
#include <autoware_auto_msgs/msg/vehicle_kinematic_state.hpp>
#include <deep_orange_msgs/msg/ct_report.hpp>
#include <motion_common/motion_common.hpp>
#include <motion_common/config.hpp>
#include <common/types.hpp>

#include <rclcpp/rclcpp.hpp>

#include <string>
#include <memory>

#include "std_msgs/msg/int32.hpp"
#include "std_msgs/msg/string.hpp"

namespace autoware
{
namespace replay_planner
{

using ReplayPlannerPtr = std::unique_ptr<autoware::replay_planner::ReplayPlanner>;
using autoware_auto_msgs::msg::Trajectory;
using State = autoware_auto_msgs::msg::VehicleKinematicState;
using motion::motion_common::Real;
using deep_orange_msgs::msg::CtReport;

/// \class ReplayPlannerNode
/// \brief ROS 2 Node for Replay Planner
class REPLAY_PLANNER_PUBLIC ReplayPlannerNode : public rclcpp::Node
{
public:
  /// \brief default constructor, starts driver
  /// \param[in] options An rclcpp::NodeOptions object to pass to rclcpp::Node
  explicit ReplayPlannerNode(const rclcpp::NodeOptions & options);

  void init();
  mutable int requested_mode = 0;

protected:
  rclcpp::Subscription<State>::SharedPtr m_ego_sub{};
  rclcpp::Publisher<Trajectory>::SharedPtr m_trajectory_pub{};
  ReplayPlannerPtr m_replay{nullptr};
  ReplayPlannerPtr m_replay_pit_exit{nullptr};
  ReplayPlannerPtr m_replay_oval{nullptr};
  ReplayPlannerPtr m_replay_pit_entry{nullptr};

private:
  rclcpp::Subscription<std_msgs::msg::Int32>::SharedPtr sub;
  rclcpp::Subscription<deep_orange_msgs::msg::CtReport>::SharedPtr received_ct_state;
  rclcpp::Publisher<std_msgs::msg::Int32>::SharedPtr pub;
  void in_num_callback(const std_msgs::msg::Int32::SharedPtr msg) const;
  void ct_state_callback(const CtReport::SharedPtr msg) const;
  std::string path = "/home/administrator/data/path_recording/oval";  // [TO DO] make as param

  void init(
    const std::string & ego_topic,
    const std::string & trajectory_topic,
    const std::string & ct_report_topic,
    const float64_t heading_weight,
    const float64_t min_record_distance,
    const std::string path_pit_exit,
    const std::string path_oval,
    const std::string path_pit_entry);

  // Subscriber  callback
  void on_ego(const State::SharedPtr & msg);
};
}  // namespace replay_planner
}  // namespace autoware

#endif  // REPLAY_PLANNER__REPLAY_PLANNER_NODE_HPP_
