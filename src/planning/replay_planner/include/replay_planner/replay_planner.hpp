// Copyright 2020 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// \copyright Copyright 2020 The Autoware Foundation
/// \file
/// \brief This file defines the replay_planner class.

#ifndef REPLAY_PLANNER__REPLAY_PLANNER_HPP_
#define REPLAY_PLANNER__REPLAY_PLANNER_HPP_

#include <replay_planner/visibility_control.hpp>
#include <autoware_auto_msgs/msg/vehicle_kinematic_state.hpp>
#include <autoware_auto_msgs/msg/trajectory.hpp>
#include <common/types.hpp>
#include <motion_common/config.hpp>
#include <rclcpp/logging.hpp>

#include <iostream>
#include <deque>
#include <string>
#include <map>

using autoware::common::types::bool8_t;
using autoware::common::types::float64_t;

namespace autoware
{
/// \brief TODO(do_12): Document namespaces!
namespace replay_planner
{

using State = autoware_auto_msgs::msg::VehicleKinematicState;
using autoware_auto_msgs::msg::Trajectory;
using Heading = decltype(decltype(State::state)::heading);


class REPLAY_PLANNER_PUBLIC ReplayPlanner
{
public:
  /// \brief Default Constructor
  ReplayPlanner();

  void readTrajectoryBufferFromFile(const std::string & replay_path);
  void IncrementA();
  void clear_record() noexcept;

  // Replay trajectory from stored plan. The current state of the vehicle is given
  // and the trajectory will be chosen from the stored plan such that the starting
  // point of the trajectory is as close as possible to this current state.
  const Trajectory & plan(const State & current_state);

  // Add a new state to the record
  void record_state(const State & state_to_record);

  // Return the number of currently-recorded State messages
  std::size_t get_record_length() const noexcept;

  // Heading weight configuration
  void set_heading_weight(float64_t heading_weight);
  float64_t get_heading_weight();
  void set_min_record_distance(float64_t min_record_distance);
  float64_t get_min_record_distance() const;
  void set_mode(int desired_mode);
  int get_mode();
  void set_speed(float desired_speed);
  float get_speed();
  int a = 5;

private:
  REPLAY_PLANNER_LOCAL const Trajectory & from_record(const State & current_state);
  REPLAY_PLANNER_LOCAL std::size_t get_closest_state(const State & current_state);

  std::size_t m_traj_start_idx{};
  std::size_t m_traj_end_idx{};
  std::deque<State> m_record_buffer;
  Trajectory m_trajectory{};
  rclcpp::Logger m_logger;

  // Weight of heading in computations of differences between states
  float64_t m_heading_weight = 0.1;
  float64_t m_min_record_distance = 0.0;
  int mode = 0;         // 0 rest, 1 pit exit, 2 oval, 3 pit entry
  float speed = 0.0;    // m/s
};

}  // namespace replay_planner
}  // namespace autoware


#endif  // REPLAY_PLANNER__REPLAY_PLANNER_HPP_
