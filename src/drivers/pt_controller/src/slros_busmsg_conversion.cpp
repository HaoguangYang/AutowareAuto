#include "slros_busmsg_conversion.h"


// Conversions between SL_Bus_autoware_auto_msgs_Complex32 and autoware_auto_msgs::msg::Complex32

void convertFromBus(autoware_auto_msgs::msg::Complex32& msgPtr, SL_Bus_autoware_auto_msgs_Complex32 const* busPtr)
{
  const std::string rosMessageType("autoware_auto_msgs/Complex32");

  msgPtr.imag =  busPtr->imag;
  msgPtr.real =  busPtr->real;
}

void convertToBus(SL_Bus_autoware_auto_msgs_Complex32* busPtr, const autoware_auto_msgs::msg::Complex32& msgPtr)
{
  const std::string rosMessageType("autoware_auto_msgs/Complex32");

  busPtr->imag =  msgPtr.imag;
  busPtr->real =  msgPtr.real;
}


// Conversions between SL_Bus_autoware_auto_msgs_TrajectoryPoint and autoware_auto_msgs::msg::TrajectoryPoint

void convertFromBus(autoware_auto_msgs::msg::TrajectoryPoint& msgPtr, SL_Bus_autoware_auto_msgs_TrajectoryPoint const* busPtr)
{
  const std::string rosMessageType("autoware_auto_msgs/TrajectoryPoint");

  msgPtr.acceleration_mps2 =  busPtr->acceleration_mps2;
  msgPtr.front_wheel_angle_rad =  busPtr->front_wheel_angle_rad;
  convertFromBus(msgPtr.heading, &busPtr->heading);
  msgPtr.heading_rate_rps =  busPtr->heading_rate_rps;
  msgPtr.lateral_velocity_mps =  busPtr->lateral_velocity_mps;
  msgPtr.longitudinal_velocity_mps =  busPtr->longitudinal_velocity_mps;
  msgPtr.rear_wheel_angle_rad =  busPtr->rear_wheel_angle_rad;
  convertFromBus(msgPtr.time_from_start, &busPtr->time_from_start);
  msgPtr.x =  busPtr->x;
  msgPtr.y =  busPtr->y;
}

void convertToBus(SL_Bus_autoware_auto_msgs_TrajectoryPoint* busPtr, const autoware_auto_msgs::msg::TrajectoryPoint& msgPtr)
{
  const std::string rosMessageType("autoware_auto_msgs/TrajectoryPoint");

  busPtr->acceleration_mps2 =  msgPtr.acceleration_mps2;
  busPtr->front_wheel_angle_rad =  msgPtr.front_wheel_angle_rad;
  convertToBus(&busPtr->heading, msgPtr.heading);
  busPtr->heading_rate_rps =  msgPtr.heading_rate_rps;
  busPtr->lateral_velocity_mps =  msgPtr.lateral_velocity_mps;
  busPtr->longitudinal_velocity_mps =  msgPtr.longitudinal_velocity_mps;
  busPtr->rear_wheel_angle_rad =  msgPtr.rear_wheel_angle_rad;
  convertToBus(&busPtr->time_from_start, msgPtr.time_from_start);
  busPtr->x =  msgPtr.x;
  busPtr->y =  msgPtr.y;
}


// Conversions between SL_Bus_autoware_auto_msgs_VehicleKinematicState and autoware_auto_msgs::msg::VehicleKinematicState

void convertFromBus(autoware_auto_msgs::msg::VehicleKinematicState& msgPtr, SL_Bus_autoware_auto_msgs_VehicleKinematicState const* busPtr)
{
  const std::string rosMessageType("autoware_auto_msgs/VehicleKinematicState");

  convertFromBus(msgPtr.delta, &busPtr->delta);
  convertFromBus(msgPtr.header, &busPtr->header);
  convertFromBus(msgPtr.state, &busPtr->state);
}

void convertToBus(SL_Bus_autoware_auto_msgs_VehicleKinematicState* busPtr, const autoware_auto_msgs::msg::VehicleKinematicState& msgPtr)
{
  const std::string rosMessageType("autoware_auto_msgs/VehicleKinematicState");

  convertToBus(&busPtr->delta, msgPtr.delta);
  convertToBus(&busPtr->header, msgPtr.header);
  convertToBus(&busPtr->state, msgPtr.state);
}


// Conversions between SL_Bus_builtin_interfaces_Duration and builtin_interfaces::msg::Duration

void convertFromBus(builtin_interfaces::msg::Duration& msgPtr, SL_Bus_builtin_interfaces_Duration const* busPtr)
{
  const std::string rosMessageType("builtin_interfaces/Duration");

  msgPtr.nanosec =  busPtr->nanosec;
  msgPtr.sec =  busPtr->sec;
}

void convertToBus(SL_Bus_builtin_interfaces_Duration* busPtr, const builtin_interfaces::msg::Duration& msgPtr)
{
  const std::string rosMessageType("builtin_interfaces/Duration");

  busPtr->nanosec =  msgPtr.nanosec;
  busPtr->sec =  msgPtr.sec;
}


// Conversions between SL_Bus_builtin_interfaces_Time and builtin_interfaces::msg::Time

void convertFromBus(builtin_interfaces::msg::Time& msgPtr, SL_Bus_builtin_interfaces_Time const* busPtr)
{
  const std::string rosMessageType("builtin_interfaces/Time");

  msgPtr.nanosec =  busPtr->nanosec;
  msgPtr.sec =  busPtr->sec;
}

void convertToBus(SL_Bus_builtin_interfaces_Time* busPtr, const builtin_interfaces::msg::Time& msgPtr)
{
  const std::string rosMessageType("builtin_interfaces/Time");

  busPtr->nanosec =  msgPtr.nanosec;
  busPtr->sec =  msgPtr.sec;
}


// Conversions between SL_Bus_geometry_msgs_Quaternion and geometry_msgs::msg::Quaternion

void convertFromBus(geometry_msgs::msg::Quaternion& msgPtr, SL_Bus_geometry_msgs_Quaternion const* busPtr)
{
  const std::string rosMessageType("geometry_msgs/Quaternion");

  msgPtr.w =  busPtr->w;
  msgPtr.x =  busPtr->x;
  msgPtr.y =  busPtr->y;
  msgPtr.z =  busPtr->z;
}

void convertToBus(SL_Bus_geometry_msgs_Quaternion* busPtr, const geometry_msgs::msg::Quaternion& msgPtr)
{
  const std::string rosMessageType("geometry_msgs/Quaternion");

  busPtr->w =  msgPtr.w;
  busPtr->x =  msgPtr.x;
  busPtr->y =  msgPtr.y;
  busPtr->z =  msgPtr.z;
}


// Conversions between SL_Bus_geometry_msgs_Transform and geometry_msgs::msg::Transform

void convertFromBus(geometry_msgs::msg::Transform& msgPtr, SL_Bus_geometry_msgs_Transform const* busPtr)
{
  const std::string rosMessageType("geometry_msgs/Transform");

  convertFromBus(msgPtr.rotation, &busPtr->rotation);
  convertFromBus(msgPtr.translation, &busPtr->translation);
}

void convertToBus(SL_Bus_geometry_msgs_Transform* busPtr, const geometry_msgs::msg::Transform& msgPtr)
{
  const std::string rosMessageType("geometry_msgs/Transform");

  convertToBus(&busPtr->rotation, msgPtr.rotation);
  convertToBus(&busPtr->translation, msgPtr.translation);
}


// Conversions between SL_Bus_geometry_msgs_Vector3 and geometry_msgs::msg::Vector3

void convertFromBus(geometry_msgs::msg::Vector3& msgPtr, SL_Bus_geometry_msgs_Vector3 const* busPtr)
{
  const std::string rosMessageType("geometry_msgs/Vector3");

  msgPtr.x =  busPtr->x;
  msgPtr.y =  busPtr->y;
  msgPtr.z =  busPtr->z;
}

void convertToBus(SL_Bus_geometry_msgs_Vector3* busPtr, const geometry_msgs::msg::Vector3& msgPtr)
{
  const std::string rosMessageType("geometry_msgs/Vector3");

  busPtr->x =  msgPtr.x;
  busPtr->y =  msgPtr.y;
  busPtr->z =  msgPtr.z;
}


// Conversions between SL_Bus_std_msgs_Float32 and std_msgs::msg::Float32

void convertFromBus(std_msgs::msg::Float32& msgPtr, SL_Bus_std_msgs_Float32 const* busPtr)
{
  const std::string rosMessageType("std_msgs/Float32");

  msgPtr.data =  busPtr->data;
}

void convertToBus(SL_Bus_std_msgs_Float32* busPtr, const std_msgs::msg::Float32& msgPtr)
{
  const std::string rosMessageType("std_msgs/Float32");

  busPtr->data =  msgPtr.data;
}


// Conversions between SL_Bus_std_msgs_Float64 and std_msgs::msg::Float64

void convertFromBus(std_msgs::msg::Float64& msgPtr, SL_Bus_std_msgs_Float64 const* busPtr)
{
  const std::string rosMessageType("std_msgs/Float64");

  msgPtr.data =  busPtr->data;
}

void convertToBus(SL_Bus_std_msgs_Float64* busPtr, const std_msgs::msg::Float64& msgPtr)
{
  const std::string rosMessageType("std_msgs/Float64");

  busPtr->data =  msgPtr.data;
}


// Conversions between SL_Bus_std_msgs_Header and std_msgs::msg::Header

void convertFromBus(std_msgs::msg::Header& msgPtr, SL_Bus_std_msgs_Header const* busPtr)
{
  const std::string rosMessageType("std_msgs/Header");

  convertFromBusVariablePrimitiveArray(msgPtr.frame_id, busPtr->frame_id, busPtr->frame_id_SL_Info);
  convertFromBus(msgPtr.stamp, &busPtr->stamp);
}

void convertToBus(SL_Bus_std_msgs_Header* busPtr, const std_msgs::msg::Header& msgPtr)
{
  const std::string rosMessageType("std_msgs/Header");

  convertToBusVariablePrimitiveArray(busPtr->frame_id, busPtr->frame_id_SL_Info, msgPtr.frame_id, slros::EnabledWarning(rosMessageType, "frame_id"));
  convertToBus(&busPtr->stamp, msgPtr.stamp);
}


// Conversions between SL_Bus_std_msgs_UInt8 and std_msgs::msg::UInt8

void convertFromBus(std_msgs::msg::UInt8& msgPtr, SL_Bus_std_msgs_UInt8 const* busPtr)
{
  const std::string rosMessageType("std_msgs/UInt8");

  msgPtr.data =  busPtr->data;
}

void convertToBus(SL_Bus_std_msgs_UInt8* busPtr, const std_msgs::msg::UInt8& msgPtr)
{
  const std::string rosMessageType("std_msgs/UInt8");

  busPtr->data =  msgPtr.data;
}

