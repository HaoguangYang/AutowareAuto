#ifndef GRACEFUL_STOP_HPP
#define GRACEFUL_STOP_HPP

#include <string>
#include <math.h>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float32.hpp"
#include "std_msgs/msg/u_int8.hpp"
#include "std_msgs/msg/bool.hpp"
#include "novatel_oem7_msgs/msg/bestpos.hpp"
#include "novatel_oem7_msgs/msg/bestvel.hpp"

namespace control 
{

class GracefulStop : public rclcpp::Node
{
    public:
        GracefulStop();

    private:
        void receiveStopCmd(const std_msgs::msg::Bool::SharedPtr msg);
        void receiveBestgnsspos(const novatel_oem7_msgs::msg::BESTPOS::SharedPtr msg);
        void receiveBestvel(const novatel_oem7_msgs::msg::BESTVEL::SharedPtr msg);

        rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr subStopCmd_;
        std::shared_ptr<rclcpp::SyncParametersClient> long_control_client;
        std::shared_ptr<rclcpp::SyncParametersClient> kin_control_client;
        rclcpp::Subscription<novatel_oem7_msgs::msg::BESTPOS>::SharedPtr subBestgnsspos_;
        rclcpp::Subscription<novatel_oem7_msgs::msg::BESTVEL>::SharedPtr subBestvel_;

        bool stop_enable = false;
        bool checks_enable = false;

}; //class

} //namespace

#endif