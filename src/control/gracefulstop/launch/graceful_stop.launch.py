import launch
import launch_ros.actions
import ament_index_python
import os

def generate_launch_description():

    # Long Control Node
    control = launch_ros.actions.Node(
        package='long_control',
        node_executable='long_control',
        remappings=[
            ("/joystick/gear_cmd", "/joystick/control_gear"),
            ("/joystick/accelerator_cmd", "/joystick/control_acc"),
            ("/joystick/brake_cmd", "/joystick/control_brake"),
        ])

    # joystick translator node
    graceful_stop = launch_ros.actions.Node(
        package='gracefulstop',
        node_executable='gracefulstop_node',
        )

    ld = launch.LaunchDescription([
        control,
        graceful_stop])
    return ld
