/* Copyright 2021 Will Bryan

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
*/

#include <kin_control.hpp>

namespace control
{

void KinControl::controlCallback()
{
  rclcpp::Time control_time = rclcpp::Clock().now();
  rclcpp::Duration time_diff = control_time - this->recv_time_;
  double dt = static_cast<double>(time_diff.seconds()) + static_cast<double>(time_diff.nanoseconds())*1e-9;

  if ( dt < 0.5)
  {
    calculateFFW();
    calculateFB();
    calculateSteeringCmd();
  }
  else
  {
    RCLCPP_DEBUG(this->get_logger(), "%s\n", "Have not received new path in > 0.5s !");
    setCmdsToZeros();
  }

  publishSteering();
  publishDebugSignals();
}

void KinControl::calculateFFW()
{
  // Desired yaw rate from feedforward
  this->feedforward_ = 0.0; // can add feedforward with a curvature: this->speed_ * this->curvature_;  
}

void KinControl::calculateFB()
{
  // Desired yaw rate from feedback
  double Kp = this->get_parameter("proportional_gain").as_double();
  this->feedback_ = -Kp * this->lookahead_error.data;
}

void KinControl::calculateSteeringCmd()
{
  // Convert desired yaw rate to steering angle using kinematic model
  double L = this->get_parameter("vehicle.wheelbase").as_double();
  this->steering_cmd.data = (this->speed_ > 1.0) ? L * (this->feedback_ + this->feedforward_) / this->speed_ : L * (this->feedback_ + this->feedforward_);
  this->steering_cmd.data = this->steering_cmd.data*57.296*19.0/9.0; // times 19.0/9 because ratio is wrong
}

void KinControl::setCmdsToZeros()
{
  this->feedforward_ = 0.0;
  this->feedback_ = 0.0;
  this->lookahead_error.data = 0.0;
  this->curvature_ = 0.0;
  this->speed_ = 0.0;

  this->steering_cmd.data = 0.0;
}

void KinControl::publishSteering()
{
  bool auto_enabled = this->get_parameter("auto_enabled").as_bool();
  double ms = this->get_parameter("max_steer_angle").as_double();

  if(this->steering_cmd.data > ms)
  {
    RCLCPP_DEBUG(this->get_logger(), "%s\n", "Saturation Limit Max");
    this->steering_cmd.data = ms;
  }
  if(this->steering_cmd.data < -ms)
  {
    RCLCPP_DEBUG(this->get_logger(), "%s\n", "Saturation Limit Min");
    this->steering_cmd.data = -ms;
  }
  
  double steer_override_threshold = this->get_parameter("steering_override_threshold").as_double();
  // IF NOT auto_enabled, always publish override command. ELSE publish steering or override command.
  if (!auto_enabled) 
  {
    this->steering_cmd.data = this->steering_override_;
  } 
  else
  {
    this->steering_cmd.data = (fabs(this->steering_override_) >= steer_override_threshold) ? this->steering_override_ : this->steering_cmd.data;
  }

  pubSteeringCmd_->publish(this->steering_cmd);
}

void KinControl::publishDebugSignals()
{
  pubLookaheadError_->publish(this->lookahead_error);
  pubLatError_->publish(this->lat_error);
}

void KinControl::receivePath(const nav_msgs::msg::Path::SharedPtr msg)
{
  // Determines lookahead distance based on speed and bounds
  double min_la = this->get_parameter("min_lookahead").as_double();
  double max_la = this->get_parameter("max_lookahead").as_double();
  double la_ratio = this->get_parameter("lookahead_speed_ratio").as_double();
  double lookahead_distance = std::max(min_la, std::min(max_la, this->speed_*la_ratio));

  // Unpacks the message and finds the index correlated to the lookahead distance
  std::vector<geometry_msgs::msg::PoseStamped> path = msg->poses;
  int idx = findLookaheadIndex(path, lookahead_distance);

  // Sets the lookahead and lateral error
  if (path.size() > 0)
  {
    this->lookahead_error.data = path[idx].pose.position.y;
    this->lat_error.data = path[0].pose.position.y;
  }
  else
  {
    this->lookahead_error.data = 0.0;
  }

  this->recv_time_ = rclcpp::Clock().now();
}

int KinControl::findLookaheadIndex(std::vector<geometry_msgs::msg::PoseStamped> refPath, double desLookaheadValue)
{
  // Finds first path pose that has x distance > lookahead distance
  for (long unsigned int i=0; i< refPath.size(); i++)
  {
    if (refPath[i].pose.position.x >= desLookaheadValue)
    {
      return i;
    }
  }
  return refPath.size()-1;
}

void KinControl::receiveVelocity(const raptor_dbw_msgs::msg::WheelSpeedReport::SharedPtr msg)
{
  const double kphToMps = 1.0/3.6;
  double rear_left = msg->rear_left;
  double rear_right = msg->rear_right;
  this->speed_ = (rear_left + rear_right)*0.5*kphToMps; // average wheel speeds (kph) and convert to m/s
}

void KinControl::receiveJoySteer(const deep_orange_msgs::msg::JoystickCommand::SharedPtr msg)
{
  this->steering_override_ = msg->steering_cmd;
}

} // end namespace control

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<control::KinControl>());
  rclcpp::shutdown();
  return 0;
}
