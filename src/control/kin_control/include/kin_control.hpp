/* Copyright 2021 Will Bryan

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
*/

#ifndef KIN_CONTROL_HPP
#define KIN_CONTROL_HPP

#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <math.h>
#include <utility>
#include <vector>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float64.hpp"
#include "std_msgs/msg/float32.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "nav_msgs/msg/path.hpp"
#include "raptor_dbw_msgs/msg/wheel_speed_report.hpp"
#include "deep_orange_msgs/msg/joystick_command.hpp"

namespace control
{

class KinControl : public rclcpp::Node
{
  public:
    KinControl() : Node("KinControlNode")
    {
      // setup QOS to be best effort
      auto qos = rclcpp::QoS(rclcpp::QoSInitialization(RMW_QOS_POLICY_HISTORY_KEEP_LAST, 1));
      qos.best_effort();

      // Publishers
      pubSteeringCmd_ = this->create_publisher<std_msgs::msg::Float32>("/joystick/steering_cmd", 1);
      pubLookaheadError_ = this->create_publisher<std_msgs::msg::Float64>("lookahead_error", 10);
      pubLatError_ = this->create_publisher<std_msgs::msg::Float64>("lateral_error", 10);

      // Subscribers
      subPath_ = this->create_subscription<nav_msgs::msg::Path>(
        "/target_path", 1, std::bind(&KinControl::receivePath, this, std::placeholders::_1));
      subVelocity_ = this->create_subscription<raptor_dbw_msgs::msg::WheelSpeedReport>(
        "/raptor_dbw_interface/wheel_speed_report", qos, std::bind(&KinControl::receiveVelocity, this, std::placeholders::_1));
      subJoySteering_ = this->create_subscription<deep_orange_msgs::msg::JoystickCommand>(
        "/joystick/command", qos, std::bind(&KinControl::receiveJoySteer, this, std::placeholders::_1));

      // Create Timer
      control_timer_ = this->create_wall_timer(
        std::chrono::milliseconds(10), std::bind(&KinControl::controlCallback, this));

      // Parameters
      this->declare_parameter("min_lookahead", 4.0);
      this->declare_parameter("max_lookahead", 50.0);
      this->declare_parameter("lookahead_speed_ratio", 0.75);
      this->declare_parameter("proportional_gain", 0.2);
      this->declare_parameter("vehicle.wheelbase", 2.97);
      this->declare_parameter("max_steer_angle", 30.0); // 15 deg * 2 because ratio is wrong
      this->declare_parameter("steering_override_threshold", 4.0);
      this->declare_parameter("auto_enabled", false);
      
      lookahead_error.data = 0.0;
      lat_error.data = 0.0;
      steering_cmd.data = 0.0;
    };

    std_msgs::msg::Float32 steering_cmd;
    std_msgs::msg::Float64 lookahead_error;
    std_msgs::msg::Float64 lat_error;

  private:
    void controlCallback();
    void calculateFFW();
    void calculateFB();
    void calculateSteeringCmd();
    void setCmdsToZeros();
    void publishSteering();
    void publishDebugSignals();
    void receivePath(const nav_msgs::msg::Path::SharedPtr msg);
    int findLookaheadIndex(std::vector<geometry_msgs::msg::PoseStamped> refPath, double desLookaheadValue);
    void receiveVelocity(const raptor_dbw_msgs::msg::WheelSpeedReport::SharedPtr msg);
    void receiveJoySteer(const deep_orange_msgs::msg::JoystickCommand::SharedPtr msg);
    
    rclcpp::TimerBase::SharedPtr control_timer_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr pubSteeringCmd_;
    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pubLookaheadError_;
    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pubLatError_;
    rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr subPath_;
    rclcpp::Subscription<raptor_dbw_msgs::msg::WheelSpeedReport>::SharedPtr subVelocity_;
    rclcpp::Subscription<deep_orange_msgs::msg::JoystickCommand>::SharedPtr subJoySteering_;
    rclcpp::Time recv_time_;

    double curvature_ = 0.0;
    double speed_ = 0.0;
    double feedforward_ = 0.0;
    double feedback_ = 0.0;
    double steering_override_ = 0.0;

}; // end of class

} // end of namespace

#endif
